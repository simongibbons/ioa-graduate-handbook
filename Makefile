all: build

build:
	latexmk -pdf handbook

preview: build
	latexmk -pdf -pvc handbook

clean:
	latexmk -C handbook
	rm -f *~ *.aux chapters/*~ chapters/*.aux photos/*~
