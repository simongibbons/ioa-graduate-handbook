%!TEX root = ../handbook.tex
\section{Evaluation}\label{eval}
 
All new Ph.D.\ students \footnote{MPhil students have their own mode of evaluation and will be provided with a similar document, outlining the relevant details, on arrival.} at the University of Cambridge are formally registered for a Ph.D.\ (Doctor of Philosophy) degree only at the end of their first year, subject to satisfactory performance --- thus the first year is to be regarded as a probationary period for the three-year extent of your Ph.D studies.
 
All of us, that is the University of Cambridge, the IoA, your supervisors and yourself, share a common goal: that you complete your thesis work and obtain your Ph.D.\ within the period of your funding. The majority of Ph.D.\ students, including those with funding via STFC, will be aiming to submit their theses within three-years.  Only the occasional student with funding from overseas, that extends to four years, will be aiming for a somewhat longer timescale (but very definitely within four years).  Finishing within the allotted time will also be viewed favourably by potential employers.  This is not an easy task and will require dedication and hard work on the part of everyone concerned.  The aim of the first year assessment is to ensure that you are well on your way to achieving this goal.  Thus, the formal registration process is an important threshold in your studentship, second only to your Ph.D. examination itself.
 
\subsection{The Route to Your PhD}
 
During the first Term you will consider the range of work undertaken at the IoA and discuss possible topics with potential supervisors.  A suitable topic for research and appropriate supervisor should normally be identified before the end of the first Term. 

\subsubsection{Graduate Lecture Courses}
 
The Institute has a long-standing policy of ensuring that Ph.D.\ students are given the opportunity to gain knowledge of a broad range of astrophysics, extending beyond the specific area of their Ph.D.\ research.  In collaboration with Cavendish Astrophysics, the IoA offers a suite of graduate lecture courses, which complement the longer and more formal astrophysics lectures offered as part of Part III of the Mathematics Tripos. First-year students are expected to attend 40 lectures during the year.

\subsubsection{Plagiarism}
Before writing anything at all your attention is drawn to the University regulations concerning plagiarism.  Plagiarism is defined as submitting as one's own work, irrespective of intent to deceive, that which derives in part or in its entirety from the work of others without due acknowledgement.  It is both poor scholarship and a breach of academic integrity. You should familiarise yourself with the University's \fnurl{plagiarism policy}{http://www.admin.cam.ac.uk/univ/plagiarism}. 
\subsubsection{First Year Assessment Exercise }
 
Formal registration for the Ph.D. degree occurs only after
satisfactory completion of the first year assessment exercise, which
involves the completion of an approximately 10,000 word report, to be
submitted no later than 15 July in the first year of study. Full details of format, word limit etc will be provided.

The report will always contain a proposal for the research to be
undertaken during the remaining period of study for Ph.D. but the
content of the bulk of the report is not closely defined. Students
will agree the title and outline content of their report with their
primary Supervisor during March-April of the first year of
study. Examples of the basis for the content of the report include: i)
a complete, or almost complete, research project, ii) a submitted
journal paper, or iii) an extensive critical literature review related
to the proposed Ph.D. topic. 

\subsubsection{Submission of Report }


Reports, as a pdf, should be sent by email to the Secretary of
the Degree Committee degreecommittee@ast.cam.ac.uk to arrive no later than 17:00 on 15th July 2014. The Degree Committee will appoint two
assessors, neither of whom will be a student's Supervisor. 

\subsubsection{First Year Assessment Interview}

Following appointment, two members of staff will each make a brief independent report to the Degree Committee on the evidence provided by the progress report and then hold an interview with the student to discuss the content of the report and the plan for future
research. The assessors will then submit a joint report and recommendation to the Degree Committee, covering the quality of the report and the student's registration, including any feedback they wish to provide for the student and Supervisor.  

\subsubsection{Assessors' and Supervisor's reports}
 
The student's Principal Supervisor will comment on progress in the light of the assessors' feedback and make his/her recommendation using the CamSIS supervision reporting system.  The Department will then consider the assessors' reports together with the Supervisor's recommendation and, on the strength of these, recommend the outcome to the Degree Committee, who will then send a recommendation to the Student Registry.

The student will see the assessors' report and the Supervisor's report when the assessment has been completed. 

\subsubsection{Monitoring Progress After the First Year}
 
To help the student monitor progress towards completion of their Ph.D. thesis, a progress report is also requested in the second year.  Finally, a final thesis-plan is requested six to nine months ahead of the planned thesis submission date.  In each case the report will be read by two members of staff and an interview with the student will be held to discuss the report.  Written feedback to the student and supervisors is then provided.
 
\subsubsection{The Ph.D. Submission and Viva}
 
Finally, the most important assessment of them all, your Ph.D. oral examination! The degree of Ph.D. is awarded primarily on the quality of a dissertation of not more than 60,000 words constituting a substantial contribution to original research. The thesis is assessed critically by two examiners who then conduct an oral examination upon the subject of the thesis and the general field within which it falls. One of the examiners will be a member of staff of the IoA (but not your supervisor) and the other will be from another institution. Most Ph.D. orals last between two and four hours. Normally you will be told whether the examiners are going to recommend the award of the Ph.D. or would like you to make some corrections to your thesis before making such a recommendation.
 
The administration of registrations, submission and examination are handled by the Board of Graduate Studies and by the Degree Committee of the Faculty of Physics and Chemistry. The Faculty consists of the departments of Chemistry, Materials Science, Physics and Astronomy. The \fnurl{Degree Committee webpage}{http://www.dcpc.physsci.cam.ac.uk/} provides details of the Ph.D. submission process and should be the first port of call if there are any queries regarding procedures.
 
\subsection{Recording Progress}\label{progress}
 
While the ultimate result of your time at the IoA should be an impressive bound dissertation, the Higher Education Funding Council for England (HEFCE), the Research Councils and the University now expect an on-going record of achievement, progress and development throughout your period of study. All IoA Ph.D. students are required to keep a Progress Log, covering activity relating to their Ph.D. research and a record of astronomy-related courses and presentations (both attended and given). The relevant paperwork and further information will be handed out in October and collected at the end of the academic year.  Students are also required to complete a Transferable Skills log which will be collected towards the end of May each year.
