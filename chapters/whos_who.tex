%!TEX root = ../handbook.tex
\section{Who's Who}
\label{whoswho}
 
One of the most important things for any new arrival to know is who to turn to with specific queries or problems. If these concerns are of an academic nature, then the first person for you to seek out is Vasily Belokurov (\textit{vasily@ast.cam.ac.uk}), who is responsible for postgraduate academic matters, or your College tutor. We are a friendly bunch at the IoA---you will find everyone happy to talk to you about any aspect of your studies (but don't approach them when they're up against a grant or funding deadline!). If, however, you do feel the need to talk to someone who is independent of the Institute's system of student oversight, then Cathie Clarke should be your first port of call.

There are several daily issues that you may come up against that are of a more practical nature, such as where to go to stock up your supply of pencils, how to use the Library, or (perhaps most importantly) who is in charge of the tea bill!
 
Here, we aim to outline briefly some of the key (non-academic) staff at the IoA and what they can do for you\ldots
 
\subsection{Reception}
 
Reception is located at the main entrance to the Hoyle Building. The receptionist handles the ebb and flow of the postal system, the switchboard, can scan documents (or set up your email to do so), send faxes and handles personal bills for the photocopiers, telephones, tea and post. Reception can order stationery if you can't find what you are looking for in the open stationery cupboards in each building. They will be glad to try and help out with any problem you might have in general day-to-day life in the IoA. Let Reception know if you will be away from the IoA for more than a few days (e.g. while observing or on holiday).
 
\begin{tabular}{lllllll}
  \textit{Contact}: &   & Bev Woolston & Hoyle Entrance & \textit{recept@ast.cam.ac.uk} & (3)37548 & fax (3)37523 \\
\end{tabular} 
 
\subsection{The Admin Team}
 
Virginia Bennett is the Department Administrator and Margaret Harding is the Deputy Administrator. They deputise for each other when the other is absent. Both are very useful people to get to know!
 
Virginia is responsible for overall administration of the IoA including finance, personnel, and welfare.  Margaret has particular responsibility for graduate students, buildings, office allocation and safety
 
Joy McSharry is Virginia's PA and has particular responsibility for personnel and training issues.  She coordinates the `researcher development' training provision for graduate students and others.
 
If you have any problems or concerns of a non-scientific nature during your time at the IoA you should inform Margaret and if she is unable to help directly she will direct you to the most appropriate person within the University. If you have any problems interacting with specific academic staff or have problems in your relationship with your supervisor you should also inform Margaret or Vasily Belokurov at an early stage.  Any issues regarding your office, furniture or other facilities should be discussed with Margaret as well as anything relating to health, safety or security.
 
Margaret can also advise you on travel, particularly to the various observatories we use regularly.  She also knows about eligibility to go observing, how to get permission to go, how to get the money, tickets etc.  Margaret will also deal with any financial issues such as chasing subsistence payments, identifying travel funds, loans etc.\ and paying expenses claims and conference fees.
 
Also involved in the processing of accounts and financial management is Susan Leatherbarrow (Mon-Fri, 8:00-12:30).

Margaret Harding deals with all pre- and post-admission matters for the IoA including the various examinations, assessment exercises and interviews.
 
Each autumn, Paul Hewett organises a Seminar on ``How to get an astro job''. Drawing on the experience of recently appointed postdocs and staff who themselves conduct interviews, he will cover: where to apply; producing a CV; completing job applications; interview techniques; etc. This is for all students and not just those in their final year as it is never too early to acquire these important skills.
 
\textit{Contact}:\nopagebreak
\begin{tabular}{lllll}
  Virginia Bennett	& Department Administrator 	& H59 & \textit{vbennett@ast.cam.ac.uk} & 37522\\
  Margaret Harding	& Deputy Administrator 		& H58 & \textit{meh@ast.cam.ac.uk}  & 37552\\
  Joy McSharry		& Administrator's PA 		& H60 & \textit{jpm@ast.cam.ac.uk} & 61537\\
  Susan Leatherbarrow	& Accounts Clerk 		& H55 & \textit{sl@ast.cam.ac.uk} & 39088\\
\end{tabular}
 
 
\subsection{The Secretaries}
 
The secretaries around the IoA are: Tricia Cooper (P.A. to Head of School), Debbie Peterson (P.A. to Director), Judith Moss (P.A to Prof M.J. Rees, Prof  A.C. Fabian and looks after undergraduate teaching and visitors), Paula Younger (P.A. to Prof. G.P. Efstathiou), and Goodrin Pebody (P.A. to Prof. G.F. Gilmore and Opticon Secretary), all of whom will help you to track down academic staff and are a rich source of knowledge about the Institute.  
 
\textit{Contact}:\nopagebreak
\begin{tabular}{llll}
  Goodrin Pebody    	& H46 &  66097 &\\
  Tricia Cooper 	& H48 &  37538 &\\
  Judith Moss       	& H4  &  37521 &\\
  Paula Younger   	& K12 &  37516 & \\
  Debbie Peterson   	& H41 &  66643 & \\
  
\end{tabular}
 
\subsection{Library Resources}
 
The Library has a professional librarian, Mark Hurn, available to help with enquiries between 9--5 Monday--Friday. The Library's role is to support the academic work of the IoA by the provision of appropriate information and services, and the staff are always keen to help you to make the most of the resources. During the postgraduate induction week there will be a tour of the Library and an introduction to the services offered. See section \ref{lib} for more information.
 
Mark is always very helpful and more than happy to conduct a search for difficult-to-find material and provide an Inter-Library Loan facility for items which are not available in Cambridge.
 
\textit{Contact}:\nopagebreak
\begin{tabular}{llll}
  Library office & Observatory E & \textit{ioalib@ast.cam.ac.uk} & 37537\\
  Mark Hurn & Departmental Librarian & \textit{hurnm@ast.cam.ac.uk} &\\
\end{tabular}
 
 
\subsection{Graphics Officer}\label{graphics}
\label{graph}
 
Amanda Smith is the Graphics Officer at the IoA. In other words, she can make pictures, posters (including very large ones!), diagrams etc.\ look as good as possible for presentation, publication etc. Amanda works mainly on Macs (which are fully interfaced with the IoA network and the web) using image manipulation and graphics programs such as Photoshop, Illustrator and Acrobat. She can acquire images in any format, from the web, via ftp from IoA researchers or wherever, as well as from their flat-bed, slide and transparency scanners, and combine, edit and output as required. Output can be as flat copy, up to and including A1 poster, overhead transparency, 35mm slides (from their electronic slide maker) or in any suitable file format for the web, for insertion into LaTeX, Powerpoint presentation etc.  Should you find that you need something that the IoA cannot provide internally (for example posters at sizes larger than A1) there is the University Photographic \& Illustration Service, \fnurl{\textit{PandIS}}{http://www.cam.ac.uk/cs/media/pandis/} (\textit{pandis@hermes.cam.ac.uk}, tel.\ 34889, whom Amanda is usually happy to liaise with for you.  They will need a PostScript or PDF file of the image/poster and can print it for you on a variety of sizes and types of paper such as A0 or custom sizes.  There is also the option to have the finished item encapsulated (i.e.\ encased in a thin layer of clear plastic) to protect it.  For an A1 poster the cost is from about \pounds 35, depending on ink coverage (i.e.\ a dark coloured background will make it much more expensive as well as less legible), plus \pounds 25 for encapsulation. It is sometimes recommended that you get an A3 proof to look at first, which costs about \pounds 5.  The IoA should pay, but check first -- they may not feel encapsulation is necessary, for example for a single-use poster. The time scale for printing is usually a minimum of a couple of working days, a little longer for encapsulation as this is only done on Wednesdays and Fridays, so do not leave it until the last minute. Should you have any questions concerning your poster from the production, graphic or layout point of view, please get in touch.

There are also several graphics packages that students can play around with themselves for basic figures, but when high quality, complex graphics are required, or editing and tweaking of existing stuff, the best call is the experts!
 
\begin{tabular}{llllll}
  \textit{Contact}: & Amanda Smith & Graphics Officer & O3 & \textit{ajs@ast.cam.ac.uk} & 37545\\
\end{tabular}
 
\subsection{Outreach Officer}
 
Carolin Crawford runs the public outreach programme at the IoA, so she's who to contact if you are interested in getting involved. There are plenty of opportunities : from helping with the popular weekly open evenings, hosting visits from brownie/cub/guide troops doing their astronomy badges, to giving talks to school pupils of all ages. You might have some bright ideas of new things we can do.
Carolin can also help with writing a public/astro-soc level talk, or with your communication skills; or with press releases for your amazingly exciting results (when they happen), and how to handle the resulting press interest.  
But think ahead: Carolin spends a lot of time working out of the IoA - either at her Emmanuel office or out visiting schools, so isn't always available at short notice.

\begin{tabular}{llllll}
  \textit{Contact}: & Carolin Crawford & Outreach Officer & H22 & \textit{csc@ast.cam.ac.uk} & 37510\\
\end{tabular}
