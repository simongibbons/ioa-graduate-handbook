%!TEX root = ../handbook.tex
\section{Survival Tips for Living and Working in Cambridge}
 
\subsection{Surviving your PhD}
 
Each person's experience of a PhD is different, but sooner or later everyone encounters a difficult patch and wonders ``why am I doing this?'' It is a long road, but there are many steps you can take to make life easier for yourself. For some excellent perspectives on how to handle graduate student issues such as managing your time, staying motivated, writing a thesis, and publishing papers, see:
\begin{itemize}
\item Estelle M. Phillips and D. S. Pugh, ``How to get a Ph.D.'', IoA Library.
\item Marie des Jardins, \fnurl{``How to be a Good Graduate Student''}{http://www.cs.indiana.edu/how.2b/how.2b.html}
\item Stephen Stearns, \fnurl{``Modest Advice for Graduate Students''}{http://www.eeb.yale.edu/stearns/advice.htm} (a rather pessimistic view!).
\item Raymod Huey, \fnurl{``Some Acynical Advice for Graduate Students''}{http://faculty.washington.edu/hueyrb/prospective.php}  (a counterbalancing optimistic view).
\end{itemize}
Remember if it was easy, someone would have done it already. For a more light-hearted look into the life of a post-graduate student, check out \fnurl{Piled Higher and Deeper comics}{http://www.phdcomics.com}. Try not to spend all your time reading comics though.
 
\subsection{Problems}
 
Sometimes the problems you face may turn out to be more serious than the regular bumps in the long thesis road. If you do find the pressures of a PhD are getting to you the most important thing is to seek assistance when you need it. This can come in lots of forms depending on the problem and how you would feel most comfortable. Within the department your supervisor or co-supervisor is a good port of call, particularly if it is a work related problem. Vasily Belokurov is also a good contact as he oversees all the PhD students. Some matters are also helped by going to see the support staff---particular for issues with money, Debbie Peterson and Margeret Harding are often good points of contact. 

For pastoral care the best contact point will be your college tutor. If you're having any issues that you don't feel you want to voice to the department, a college tutor can often be very helpful. They will normally be very receptive if you drop them an email to arrange a meeting. The best thing to do is not to stay quiet! If you're having problems, no matter what they are, most people in the department are very approachable and you can turn to anyone who you feel comfortable with.
 
\subsection{Practical Life Matters}
 
\subsubsection{Healthcare}
 
Register with a doctor (called general practitioners or GPs)\footnote{For Overseas Students: general practitioners work in ``surgeries'', probably known to you as medical practice offices or clinics.} and a dentist (University Dental Service: 3 Trumpington Street, 01223 332 860).  If you live in College they should help you register by suggesting a GP when you arrive.  It's pretty straightforward; overseas students should make sure to bring a passport along with their university ID card to the surgery, then they will ask you to fill out a few forms about your medical history. Although it's easy, don't forget to do it since you could run into trouble if you're not registered and have an emergency. Your GP should be your first point of contact in most circumstances.  If you find yourself sick out of surgery hours, there is a national NHS hotline number that you can call to get advice about your medical condition, routed through a local number called {\it Cam-Doc} (01223 464 242).  If necessary they might even make a housecall. If you find yourself unwell and unhappy with your GP, don't forget that switching surgery may be helpful.  For those of you living near the IoA, the Huntingdon Road Surgery is highly recommended although in town there are a variety of surgeries.
 
A good source of general information is \fnurl{NHS Direct}{http://www.nhsdirect.nhs.uk/}, this is worth consulting in many situations. Remember that NHS healthcare in Britain is free at the point of care and nearly perfectly reliable (compared to many other countries). The only thing you'll normally have to pay for is prescription medications, at \pounds 8.05 each. Some types of prescriptions are free, like birth control---for a list of who receives free prescriptions and what is free, visit the NHS Prescription Costs webpage. If you find yourself needing more than 4 prescriptions in 3 months buying an NHS pre-payment certificate could save you money.  It'll cost about \pounds 29.10 up front, but then you won't have to pay for any prescriptions that you need in the 3 month period when your certificate is valid.  12 month certificates also exist, but only offer modest savings compared to buying four 3 month certificates.
 
Some colleges in Cambridge also have college nurses for common ailments and health issues. The information is kept in confidence. Check with your college porters or on your college website to see if there is a college nurse and what hours they keep.

The local NHS hospital for Cambridge is Addenbrooke's, which can be reached from the IoA on the Uni4 bus line or from town on the Citi 1, 2, 7, 13a, or Milton/Babraham Park and Ride bus.  Addenbrooke's Accident and Emergency (A \& E) is the nearest emergency room, should you need to be seen urgently.

In case of a serious crisis, the emergency services number EU-wide is \textbf{112} or, in the UK, \textbf{999}. This will connect you to a operator who will ask you about the emergency. Stay calm and explain where you are and what's happened. It is possible to contact the police, the fire service or ambulance service through this number. You can also contact less common services like the coastguard and rescue services. \textbf{111} is a new phone number in the UK that can be used for medical help where it is not a severe emergency. Calling this is also useful if you don't know whether to go to A \& E and they will recommend a place for you to go for treatment. 
 
For going abroad, remember to apply for a European Health Insurance Card (EHIC), even if you're an overseas student.  So long as you have NHS benefits, you can apply for an EHIC, which will help with health care costs when elsewhere in the EU.  While not a substitute for travel insurance, it's definitely useful to have, especially for holidays and conferences.  If not an EU resident, you'll have to get a paper application from the Post Office and attach a copy of your passport's picture page and visa page, to your application.
 
If you are experiencing stress, depression, or anxiety, or particularly want to talk about mental health, you can speak to a nurse or a GP for more information. You can also see the \fnurl{University Counselling Services}{http://www.counselling.cam.ac.uk/} which is available to both students and staff for free. They offer short sessions of counselling, usually for 6 weeks, and can discuss any mental issues. They also offer group counselling and certain types of training. They are located on Lensfield Road, off Trumpington Street towards the south-east of Cambridge. Their waiting list can be as short as a week but does get a little longer during term time when they are more over-subscribed. Their website has more information on their service and how to apply for an appointment. The NHS also has mental health services and there are several groups in Cambridge for young people, (e.g. Centre 33) which can offer similar services for free but you may find the waiting lists are longer.
 
\subsubsection{Travelling}
 
If you need to escape from the Cambridge bubble from time to time, remember that London is only an hour away by train, and Stansted airport is even closer! Remember that if your travel is related to the Institute or your course you can speak with the Admin Department about whether you should put in a claim on expenses. 
 
\paragraph{Trains}
 
If you are under 25, get a Young Person's Railcard. This saves you 1/3 on all rail tickets and pays for itself if you go to London more than twice in a year.  It costs \pounds 30 and is available at the Cambridge station service desk or online through \fnurl{National Rail}{http://www.nationalrail.co.uk} (you will need a passport-sized photograph). Look for coupon codes and other discounts before you purchase, as it is usually possible to save 10\% or more. You can also buy a Railcard if you are over 25, but you'll need proof of your status as a full-time student.

The National Rail website also serves as a good journey planner and will send you to the rail operator's site to buy tickets.  You don't need to worry about picking the right rail operator, as you can buy any ticket on any website.  Occasionally there will be a deal that is only available through the train operator you will be travelling with, but this is the exception rather than the rule.  A good tip is to think about getting a PlusBus upgrade to your ticket, giving you free bus travel within your destination city (except London) on the day of arrival. It is often significantly cheaper than a regular day ticket.
 
A note on travel costs---always check all the options carefully. Return and advance tickets are your great friend, as they generally reduce the price significantly.  Open return tickets allow the return journey to be any time up to a month after the outward trip, which allows you to save money even if your return leg is not fully arranged. Be aware of off-peak returns as these allow you to travel back on any train for up to a month after your outward trip, but only on certain trains that are outside of peak times. These tend to be week days 8-10ish and evenings 4:30-6:30ish but it can depend. If you get caught out and end up on a peak-time train with an off-peak ticket you are normally forced into buying a peak ticket so it's best to look ahead to see which times are available to you. Train pricing works in mysterious ways; a day return ticket can sometimes be cheaper than a single fare.  What's even more bizzare, once in a blue moon the first class ticket may be cheaper than standard. 
 
Trains are extremely convenient for travelling to London.  An express connection to London King's Cross operates at high frequency.  There is also a marginally cheaper though rather slower connection to London Liverpool Street. The biggest drawback of train travel is that the station is outrageously far away from the city centre --- on Cambridge standards that is.  Think about 20 minutes walking distance from the city centre, (quite a bit further from the IoA), there are also very frequent buses between the train station and the city centre, even quite late in the evening.
 
If you are planning on travelling around London regularly, it is worth investing in an \fnurl{Oyster card}{http://oyster.tfl.gov.uk/}, which makes paying for public transport in London both cheaper and easier. These are used on buses, the underground and the overground trains in London. It is very like other metro systems in cities around the world. Your Oyster card can be topped up and then swiped on public transport to pay for a ticket. This is cheaper and easier than paying by cash. It records your journey and will take the cheapest fare possible---just make sure to swipe off at the end of your journey to avoid paying a penalty.
 
\paragraph{Coaches}
 
Coaches are often a cheaper alternative to trains and can sometimes be more convenient.  The main carrier is \fnurl{National Express}{http://www.nationalexpress.co.uk}.  If you plan to use coaches regularly, get the Young Person's Coach Card for discounted fares.  It costs \pounds 10, and you can buy it online or at any coach station (Drummer Street in Cambridge).  The long distance coach connections depart from and arrive to the east side of Parker's Piece, which is within a comfortable walking distance from the city centre.
 
There is also the infamous (and painfully slow) X5 line operated by Stagecoach, providing you with the cheapest way of getting to the Other Place\footnote{a.k.a. the Dark Side or --- extremely rarely --- \emph{Oxford}.}.  Note that despite being rather more expensive, and involving a fairly long connection on the Underground to get from King's Cross to Paddington, the train is in fact still faster by at least 30 mins.
 
\paragraph{Air travel}
 
The natural thing to do when flying is to use the London airports:
\begin{itemize}
\item Stansted (30 minutes train ride for about \pounds 7 with a railcard).
\item Luton (1hr coach ride, \pounds 12).
\item London City Airport (tiny airport actually in London, 2hr train and Tube journey \pounds 15 with railcard).
\item Heathrow (major international hub, 2hr train and Tube journey \pounds 16 with railcard or 2.5hr coach trip \pounds 28).
\item Gatwick (the furthest of the London airports, 2.5hr train journey \pounds 19 with railcard).
\item London Southend (rumoured to exist but, like the Yeti, no-one has ever actually seen it)
\end{itemize}
 
For cheap flights within Europe, check out Ryanair, Easyjet, Wizzair, BMI and LastMinute. Ryanair, despite charging you extra for pretty much everything, has the advantage of flying from Stansted.  A good website that will give you flexible dates and good travel ideas (as well as quotes from all cheap European airlines) is \fnurl{Skyscanner}{http://www.skyscanner.net}.  Finally, one crucial hint for flying with ``cheap'' airlines --  don't go over the luggage weight limit, you will soon realize that British Airways would have been cheaper. Makes sure your hand luggage also meets their requirements, these can change even between different budget airlines. \footnote{There's also Cambridge ``International'' Airport if you'd like to fly to Dublin...apparently the airport is just past Tescos!}


\paragraph{Car rental}
 
Most people consider keeping a car in Cambridge hardly worth the effort, as getting around the city is practically impossible with one. If you want to rent one occasionally, Cambridge offers the standard range of big name companies.  If you are between 23 and 25, \fnurl{Cambridge Car and Van Rental}{http://www.cambridgecarandvanrental.co.uk/} is one of the few places that will not charge you a young driver surcharge. Also, you get a free day on every 5th rental and a 10\% discount for University members. If you think you may need a car more frequently for short trips, you may be interested in looking at \fnurl{Street Car}{http://www.streetcar.co.uk/}. Their nearest spot is Churchill College, 5 minutes from the department.

\paragraph{Taxis}

The main taxi company in Cambridge seems to be Panther Taxis. They are fairly reliable and you can book a taxi on 01223715715. The department also has an account with Panther Taxis so if you are on university business with the IoA speak with Debbie Peterson or Reception about booking a taxi through the Institute's account.
 
\subsubsection{Getting around the town}
 
Many people walk or take the bus in order to get to the IoA. As of the time of writing, The Uni4 bus (and Citi4 where its route overlaps with the Uni4) going past the department are less than a pound per ride if you show your university ID.  
 
Still, the Cambridge way to get around is cycling. Some colleges have bike sales at the beginning of the year.  These are substantially cheaper than local shops and worth getting to before the undergraduates come back.  For a cheap new bike, check out Station Cycles (next to the Grand Arcade car park entrance, in the street behind the Material Sciences department) or the cycle workshop in the narrow alleyway between the Anchor and the Mill (both pubs, if you're wondering).   You can also check out the adverts list on the \fnurl{University forums}{http://forum.cam.ac.uk} where people post things they want to sell.  Don't forget when you get your bike to get a good helmet and a good lock.  If you live in College housing, you should also register your bike with them so they know not to throw your bike away when you're gone on holiday! You can also register your bike with \fnurl{Immobilise}{http://www.immobilise.com}) which is a UK wide property register in case your bike is stolen.  

If you decide to ride at night, note that you are legally required to have lights: one white light in the front and a red light for the back. You will be fined if you're caught without them. It is also illegal to ride a bicycle on the pavement except where signs permit it. Otherwise, cycle on the left of the road. In Cambridge there are several roads with designated cycle paths. Note also that bicycles are bound by the rules of the road and must obey all the stop signs and traffic lights just as other vehicles would. That won't stop many of the cyclists you see around town, but one day they will be caught and ticketed in front of your law-abiding self, and you will have the satisfaction of knowing that you did The Right Thing.
