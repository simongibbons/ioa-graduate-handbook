%!TEX root = ../handbook.tex
\section{Computers}\label{computers}

The Institute's Science Cluster will most likely cater for your day-to-day computing needs during your time here. The cluster consists of computers ranging from individual desktops to shared servers and is \textsc{unix} based with most machines running Red Hat Linux. When you first arrive at the IoA there will be a `computer' of some kind on your desk to get you started. In January new students and their supervisors will be surveyed to ensure that anyone with specific computing requirements for their research can get a machine that meets their needs. These machines should be deployed by the end of March.

Details of the computer system are available within the \fnurl{Computing section on the IoA Intranet}{http://local.ast.cam.ac.uk/computing}, which is accessible from IoA machines or through Raven (see Section \ref{raven}). This is a pretty extensive resource detailing the machines and software available as well as computing policies. You may want to look at the FAQs, look at the \fnurl{User Guide}{http://local.ast.cam.ac.uk/computing-user-guide} which gives you step by step instructions for specific tasks on the cluster or use the intranet search to find information.

The cluster offers many software packages ranging from standard web browsers, office suites and image editors to scientific packages (Mathematica, MATLAB, \textit{etc.}) and specialist astronomical software (including ds9, GAIA, IRAF, Source EXtractor).

\subsection{Shared Machines}

While you will have your own desktop computer, you may wish to run jobs on shared servers which can offer higher performance in terms of CPU speed and memory. CPU time on these machines is a shared resource so you should \texttt{nice} all intensive jobs you run or you will rapidly make enemies! 

If you are using a SunRay thin client, your session will be running on a shared server so please be considerate. Multiple people will be using that server and if you run intensive jobs on there, those people will be able to see who's making their session grind to a halt\footnote{The \texttt{htop} command is your friend here}!

\subsection{Storage}

Your account will give you ``a reasonable amount'' of home disk space (at the time of writing 2GB), available at \texttt{/home/username}\footnote{Though you should expect to exceed your allocated home disk space on a regular basis...}. On request users with a high volume of email, software source code or documentation can have up to 6GB in chunks of 500MB or 1GB. Requests, accompanied by a short, one sentence, justification should be sent to Helpdesk (see Section \ref{helpdesk}) There are other storage disks (usually called ``data disks'') you can access for your GB/TBs of data (if you have them). If you need a ``reasonable'' amount of storage on the data disk e-mail Helpdesk (see Section \ref{helpdesk}) with your request or ask your supervisor about ``unreasonable'' amounts\footnote{If you're looking for a definition of reasonable, your guess is as good as ours...}. 

Home directories are automatically backed up daily\footnote{The backups can be found in \texttt{\textasciitilde/.zfs/snapshot}} and backups are held for a month, anything at \texttt{/data/store} or \texttt{/data/superstore} is backed up daily at midnight and only stored for one day. For any other location, no backups are kept\footnote{If you are going to be a member of the X-Ray Group, things are a little different: Backups are done automatically every day for you there, which is kinda cool.}. 

In \textsc{unix}, the \texttt{rm} (delete) command is unforgiving and will delete the file immediately -- at some point you \textbf{will} delete something important, so be prepared! It is usually safer to use `\texttt{rm -i}' to confirm before you delete if you're not sure. If you do have anything important, it is your responsibility to make sure it is backed up. CDs, DVDs, Blu-ray Discs and USB memory sticks may be obtained from Helpdesk for this.

\subsection{E-mail}

You will have a departmental e-mail address, \textit{username@ast.cam.ac.uk}, which is separate from your central University Hermes account. Correspondence from the department, \textit{e.g.} newsletters, and other e-mails will be sent here as well as any information about your IoA Cluster account. You can access your e-mail through IMAP using software such as \fnurl{Thunderbird}{http://local.ast.cam.ac.uk/computing-user-guide/configuring-personal-computers/configuring-email-clients}, the \fnurl{webmail interface}{http://webmail.ast.cam.ac.uk}, or the terminal of any cluster machine (using \texttt{alpine} or \texttt{mail}).

If you want to forward your departmental e-mail to another address, create a file called \texttt{.forward} in your home directory containing the e-mail address you wish to forward to.

\subsection{Personal Web Pages}

If you make a folder called \texttt{public\_html} in your home directory, you will be able to create your own personal web-pages using HTML, PHP and CSS, accessible at \url{www.ast.cam.ac.uk/~username}. These pages are in addition to your main department website profile which you can find in the `People' section on the IoA website. To change your profile on the main website, scroll to the bottom of any page and click the `User Login' link in the footer. Your username will be the same as your main IoA account however as this is a separate system, the password is different and will need to be reset the first time you access it using the link on that page.

Although you are free to write whatever you like on both these sites, please remember they are IoA and University of Cambridge websites, so don't write anything that might upset either! Violation of the rules will lead to the removal of your pages.

\subsection{Raven}
\label{raven}
Your Raven ID (provided by the central university) is used to authenticate you onto a number of web based services around the University. It should automatically be added to allow you access remotely to the department intranet and room booking system, however if you have any problems, please e-mail Helpdesk.

As a graduate student, you should be given your Raven ID and password by your college.

\subsection{Remote Access}

A small group of machines may be accessed from outside the IoA (see \fnurl{``Remote Access \& Security''}{http://local.ast.cam.ac.uk/computing/remote-access-security}), but only via secure shell (SSH), \texttt{scp} or \texttt{sftp}. NoMachine NX is also available which gives you a graphical interface to a remote machine rather than just a terminal (although you need to connect via the \fnurl{VPDN service}{http://www.cam.ac.uk/cs/remote/vpdn.html}).

Free SSH clients are readily available for Windows (PuTTY) and most other operating systems and should come built in to Linux and Mac OS X. Linux and Mac clients allow X-forwarding (by using the \texttt{-X} option), while X-forwarding is possible on Windows clients using an X-server such as the freely available Xming.\footnote{Another alternative for windows is \textit{mobaxterm}(http://mobaxterm.mobatek.net/) which can do X graphics as well as SSHing.} If you don't understand all this \textsc{unix} terminology, then don't worry, you will soon pick it up and there is more information on the \fnurl{intranet}{http://local.ast.cam.ac.uk/computing-user-guide/logging-remotely-using-secure-shell-ssh}.

\subsection{Laptops}

We provide desktop machines for all students, but do not provide full specification laptops.  The department can loan modest specification laptops to student for use as `screens' to facilitate remote access to the Institute computer system.  For further information, please contact Helpdesk.

\subsubsection{Connecting Your Laptop}

If you want to connect your own laptop to the IoA network with a \textit{wired} connection you will need to complete a form available from Helpdesk. Once this is complete you should be able to your laptop into most of the network ports on site.\footnote{Note that not all the network ports are enabled. If you plug into one and nothing happens try another one or contact helpdesk to get that port enabled.}

\subsubsection{Wireless Networking}

Wireless access is provided by the \fnurl{\textit{eduroam} service}{http://www.ucs.cam.ac.uk/wireless/eduroam/} which will allow you access WiFi in Cambridge, as well as worldwide at most universities. It can be difficult to set up when outside of Cambridge, so it would be a good idea to do it before you leave. Eduroam will also work on mobile phones.

There is also the \fnurl{\textit{UniOfCam}}{http://www.ucs.cam.ac.uk/wireless/browser/} network which allows you to quickly connect to the internet alone whilst in Cambridge. Once you are connected to the network you will need to open a web browser and then log in using your Raven ID.

\subsection{Printing}

Printing using the deparment's printers is free for research related work. You can find out which are the nearest printers to your office by either asking your office mates or looking it up on the intranet. If your laptop is connected into the department network or \textit{eduroam}, you can also use that to \fnurl{print to printers in the department}{http://local.ast.cam.ac.uk/computing/resources/printers}.

Quite often when you go to a conference you will end up presenting a poster summarising some of your recent work. You can find out more about getting these printed in Section \ref{graphics}.

\subsection{When it all goes wrong...}

The first thing you should do in the event of any computer problem is of course RTFM\footnote{Read the fine manual. Ahem\ldots}. You should also look at the \fnurl{Computing User Guide}{http://local.ast.cam.ac.uk/computing-user-guide} in the intranet even if you are familiar with computers. If this does not answer your question, try asking the people in your office or someone in the experts list in Section \ref{experts}\footnote{Most of them don't bite...}. If they are of no use, then the computer support ``Helpdesk'' (see below) should be able to help.

\subsection{The Computing Group \& Helpdesk}
\label{helpdesk} 
The IoA has a group of computer support staff (Karen Bailey, Andy Batey, Graham Bell, Roderick Johnstone, Sue Cowell and Neil Millar), who are in charge of the network and most of the software available on the IoA Cluster (the X-ray group has its own cluster managed by Roderick Johnstone).

The IoA Computing Group are best contacted via \fnurl{`Helpdesk'}{http://local.ast.cam.ac.uk/computing/getting-support}, which is both an actual office and an e-mail address. Helpdesk uses a support system called \fnurl{RT}{http://helpdesk.ast.cam.ac.uk} which generates `tickets' regarding incidents. It is monitored by the support team during normal working hours and the system keeps track of faults that haven't been fixed, maintains records of what has been done already and provides historical notes for methods of fixing similar problems in future. 

You can seek the advice of the Computing Group by popping round to H42 between 9:00am-12:30pm and 1:30-3:30pm (note that they are closed between 9:30-10:30am on Fridays)\footnote{We can tell you now they won't be there if it's tea or coffee time...} Monday-Friday or by phone or e-mail (see below). However, {\bf you should always submit a request/report of any problem or question to the `helpdesk' e-mail account.} An e-mail to `Helpdesk' ensures that i) your problem is formally logged, ii) a specific response will be forthcoming, iii) the knowledge about your problem reaches the appropriate group member rapidly and iv) the Computer Group don't end up fixing the same issue multiple times for different users who have not used the `Helpdesk' e-mail facility. 

Please \textbf{do} contact helpdesk urgently with hardware problems e.g.\ if a machine has crashed or hung, and \textbf{do not} attempt any direct action of your own which could cause physical damage or data loss (a quick way to lose friends!); The other reason not to restart the system yourself unless absolutely necessary is that although that may clear the fault it is then much harder to find out what went wrong and hence prevent a repetition of the problem. \textbf{Please do not e-mail individual Computer Group members as mail directed to `Helpdesk' is much more effective and helps everyone}.
 
The standard Helpdesk number is 66666\footnote{No, this does NOT have anything to do with the devil... Or so they tell us...}. A mobile phone number (dial internally on 50339) is available for use either when the Helpdesk operator is away from the office, or as a single phone contact for out of office hours computer support. Please be sparing with the use of this facility!  Basically, if the building is in danger of exploding or your machine has become demonically possessed and is breathing fire\footnote{Although this is potentially useful for keeping your feet warm on a cold winter's evening}, then you can probably think about dialling the mobile. Remember, usually the best way to resolve a problem is to e-mail Helpdesk, even out of hours as system managers will occasionally log in. This not only allows a written record of the
problem, but logs it in the database so that it can't get lost.
 
\begin{tabular}{lllll}
  \textit{Contact}: & Helpdesk & H42 & \textit{helpdesk@ast.cam.ac.uk}
  & 66666
\end{tabular}

\subsection{Feedback}
If you do have any general comments or concerns you want to raise about computing in the IoA, the best way to do this is through the Student Reps on the Computer Users Committee (CUC). This year, these are Simon Gibbons (sljg2@ast.cam.ac.uk) and Scott Thomas (swt30@ast.cam.ac.uk) who are more than happy to be contacted about any issues\footnote{They just can't get enough of hearing about your computing related issues!}.
