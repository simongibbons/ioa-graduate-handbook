%!TEX root = ../handbook.tex
\section{Reference}
\label{reference}

\subsection{Library}
\label{lib}
 
The IoA Library, located on the ground floor of the Observatory building, holds about 35,000 books and 250 current periodical titles including electronic publications. Apart from a few special collections all material is available on open shelves. Books may be borrowed for up to three months. A printed/online guide to using the Library is available. The \fnurl{Library home page}{http://www.ast.cam.ac.uk/library/} brings together a range of astronomical and general Library resources.
 
Newton, the Library's web catalogue, contains records for all material held in the Library.  Newton is shared with other departments and faculties A-E, but by using the `Set Limits' function it can be set to search only the IoA Library. A list of new acquisitions is circulated monthly and suggestions for new titles are very welcome.  A display of new books can be found in the small library area by the directors offices in the Hoyle Building.  Monthly listings of new books can be found on the Library website. 
 
Databases and e-journal services are available via the IP address. In practice, this means the IoA or the University has taken out a subscription to a given journal, and you should be able to access the contents without needing a password from any IoA or University computer. Sometimes the system is confused by the web proxy server and will prompt you for a password rather than allowing automatic access. In this case, try bypassing the proxy (e.g.\ select ``direct connection to the Internet'' in your browser preferences). Remember to turn proxying back on afterwards!  Any access problems should be reported to the Library. In addition, the Library can provide help on searching online information services (including U.L.\ Catalogue, ADS). The IoA Publications database is managed and updated by the Library.
 
The \fnurl{Rayleigh Library}{http://www.phy.cam.ac.uk/library/} is just across the road in the Cavendish Labs, and may occasionally have something that the IoA does not. Failing that, there is also the \fnurl{Betty and Gordon Moore Library}{http://www.lib.cam.ac.uk/BGML/}, which covers sciences, in the Centre for Mathematical Sciences on Wilberforce Road and the mighty \fnurl{University Library (UL)}{http://www.lib.cam.ac.uk/}, which has every book ever published anywhere by anyone, or something like that. Individual Colleges have their own, less-specialised, libraries, which can be useful if you just want to consult a standard textbook.
 
Feel free to contact Mark Hurn (hurnm@ast.cam.ac.uk) if you have any questions about the library, or if you have suggestsions for new books.
 
\begin{figure}[h]
\centering
\hspace{2.0cm}\includegraphics[width=9cm]{./photos/IoA_observatory_1834.jpg}
\newline
\newline
\caption*{The astronomy library actually has a very nice collection of scanned images of \fnurl{pictures and photographs}{http://www.dspace.cam.ac.uk/handle/1810/214761}. This is one dating back to 1823 when the Obs building was first constructed!}
\end{figure}
 
\subsection{Post and Telephones}
 
Mail trays are located in each of the main buildings, both for standard (Royal Mail) post and the University Messenger Service (UMS). The latter is a free service that delivers letters and small parcels between university departments and colleges every weekday.
 
You can use the IoA mail trays for work-related post and already stamped personal items. Stamps for personal post can be purchased at reception, at the usual rates. The deadline for outgoing post is 4:00~pm each weekday. Special deals are available for overseas mail (especially handy at Christmas time!) - please ask Reception at the time. If you need to arrange for Recorded/Special Delivery of important documents, such as job applications, contact reception.
 
Incoming mail (addressed to you at The Institute of Astronomy, Madingley Road, Cambridge, CB3 0HA) will be delivered to your departmental pigeonhole each weekday morning after tea break. There are sets of pigeonholes in each of the main buildings -- yours will be in the building containing your office.  UMS mail is collected and delivered to the Hoyle building at 11am each weekday and will be distributed with the post.
 
Each office has a telephone, shared between all the people in that office.  Incoming calls can be made direct to your office number. As with the mail, there are two types of telephone networks, the University has an internal network of 5-digit numbers (which start with either 3 or 6) and calls between numbers on this network are free. If the 5-digit number begins with a 3, add an extra 3 at the beginning to dial it from outside the network, if it begins with a 6 you should add a leading 7.
 
Personal and business telephone calls can be made from the telephone in your office, although again any personal calls must be paid for and will be added to your bill. For calls outside the University network (UK landlines and mobiles), dial 9 first to get an outside line. You will need to dial reception (37548) to make international calls from your office phone.
 
 
\subsection{Office Equipment and use of Printers/Photocopiers}
 
There are photocopiers and fax machines at various points throughout the IoA. You should pay for any personal use at reception, photocopies are 3p per side of A4 and other prices can be found on the list there.  Printers can be found all over the site, see the User's Guide (see section \ref{computers}) to find out where they are. Personal use is allowed within reason, although if this is abused the IoA will be quick to change this. Also, remember that colour printing is VERY expensive, use the colour printers only when completely necessary.
 
Stationery cupboards with files, pens, paper etc.\ are also there for you to use, located in both the Hoyle, Kavli and Observatory buildings.  If you can't find what you're looking for, please let reception know. If you require any special arrangements to be made for your office setup (new office chair, non-fluorescent lamp, keyboard, etc.) please see Joy McSharry.
 
\subsection{Cambridge Terminology}
 
This is complicated.  There are so many things to know about the University.  The most important things that will likely be related to your experience are the following:
 
\begin{itemize}
\item There are \fnurl{3 Cambridge Terms}{http://www.cam.ac.uk/univ/termdates.html}, called {\sc Michaelmas} (beginning October to the end of November), {\sc Lent} (mid-January to mid-March), and {\sc Easter} (mid-April to mid-June) These are busier times of the year, when pesky undergraduates take over the town.  It is also a busier time at the department, with colloquia and graduate courses and more people around.  Try to avoid taking holiday during these times.
\item You are a member of a Cambridge College.  Make sure you know this and matriculate at the start of term (early October).  Why point out something ``so obvious''?  Without mentioning names, one of your peers forgot to matriculate some years ago, leading to excessive headaches when trying to submit his first year report.
\item As a member of the University you have an email under the domain {\it @cam.ac.uk}, which is different from your {\it @ast.cam.ac.uk} account. Your {\sc RAVEN} login is also your cambridge user ID ({\it @cam.ac.uk}), and is needed to access a number of Departmental and University services.
\item Check out the \fnurl{Cambridge discussion forum}{http://forum.cam.ac.uk} for things on sale or free, or advertise your stuff to the University community.
\end{itemize}
 
\subsection{International Students}
 
The IoA is a truly international institution, so you have the opportunity 
to meet people from all over the world! Overseas students might have 
particular queries about life in Cambridge or the UK in general, so
here is a list of the current international students at the IoA, together 
with their country of origin, year and email id. If you see a compatriot on the list 
they would no doubt be glad to hear from you! 

\begin{center}
  \begin{tabularx}{0.7\linewidth}{X l l l}
    \hline
    \textbf{Student}         & \textbf{Home Country} & {\bf Year} & \textbf{IOA email}\\
    \hline
    Nadia Blagorodnova             & Spain               & 4        & nblago\\
    Tiago Andre Costa              & Portugal            & 4        & taf34\\
    Alvin Chua                     & Singapore           & 2        & ajkc3\\
    Jonathan Diaz                  & USA                 & 4        & jdiaz\\
    Stefano Facchini               & Italy               & 3        & facchini\\
    Keith Hawkins                  & USA                 & 2        & kh536\\
    Shaoran Hu                     & China               & 2        & sh759\\
    Erin Kara                      & USA                 & 3        & ekara\\
    Harley Katz                    & USA                 & 2        & hk380\\
    Laura Keating                  & Ireland             & 2        & lck35\\
    Patricia Larsen                & New Zealand         & 2        & prl37\\
    Kaloian Lozanov                & Bulgaria            & 2        & kd309\\
    Luca Matra                     & Italy               & 2        & lm605\\
    Fernanda Ostrovski             & Brazil              & 2        & fo250\\
    Electra Panagoulia             & Greece              & 4        & caillean\\
    Iulia Simion                   & Romania             & 4        & isimion\\
    Scott Thomas                   & New Zealand         & 2        & swt30\\
    Gabriel Torrelba               & Chile               & 2        & git21\\
    \hline
  \end{tabularx}
\end{center}
