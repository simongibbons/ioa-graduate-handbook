%!TEX root = ../handbook.tex
\section{Travel and Money Issues (for PhD students)}
 
\subsection{Travel Funds}
 
\textbf{Conferences, Workshops and Meetings}
\newline
This is applicable to all students, Home and Overseas, unless your sponsor (e.g. Gates Cambridge) specifically provides direct funding.
 
\fnurl{STFC}{http://www.scitech.ac.uk/Grants/Studs/studentships.aspx} and some other sponsors provide the Department with a grant to support your research (note that some sponsors will require you to make a specific application before each trip and will reimburse you directly). These funds are intended to cover all the resources, facilities and travel you will require. The IoA pools these grants and dedicates the resultant funds primarily to student travel, thus you do not have a personal travel budget but can expect support from the IoA along the following lines:
 
\begin{itemize}
\item For the duration of your studentship, a total sum of \pounds 2,500 to include conferences, workshops and meetings.  
\item If your sponsor does not provide a total sum of \pounds 2,500, the IoA will cover the shortfall.
\end{itemize}
 
This is a fairly generous allowance but is not to be seen as a personal allocation. Use the money wisely! You may find you need to save more money for your final year when going to conferences to look for jobs.
 
You will need to have your supervisor's approval for any trips and then check with Debbie Peterson (or, in her absence, with Margaret Harding) in Hoyle H12 what funds can be provided before making any commitment. We try to be very flexible but you should also take advantage of other sources of funding or potential contributions from your College, from the organisers of conferences and from other possible sources. If another institution has promised to reimburse part or all of your expenditure it is generally possible to arrange for the IoA to pay the full cost up front and for you to reimburse her when you receive the money.
 
\subsection{Arranging Travel}
When arranging travel to conferences, \textit{etc.}, you will first need to work out roughly how much the trip will cost, including flights, travel to the airport, accommodation, food, \textit{etc.}, and registration fees for conferences (these can often be paid up-front by the IoA), then check with Debbie Peterson what funds are available before making any commitment.
 
Make sure you check your \textbf{passport} and \textbf{visa} requirements, if any, as a first step.

\textbf{Flights} may be booked via STA Travel as the University has an account with them. Key Traveller is also an option. Debbie Peterson can provide a purchase order that can be scanned and emailed to the travelling agency so the IoA pays for the flights directly. If  the travelling agency does not cover your airline of choice (\textit{e.g.} it may be more appropriate to travel with a budget airline; EasyJet or RyanAir, which are not covered by STA), you should book the flights yourself then claim the money back on expenses.
 
\textbf{Travel insurance} must be obtained through the University. University travel insurance is available free of charge to all staff and graduate students travelling on University business. To obtain travel insurance, enter your travel plans on the \fnurl{online travel insurance registration system}{http://www.admin.cam.ac.uk/offices/insurance/travel} (select `Application Process for Graduate Students registered with the University of Cambridge by The Board of Graduate Studies'). Full details of the policy are available on this page and holiday can be included on the policy so long as it counts for less than half of the period you are away. After arranging travel insurance, you should forward the confirmation e-mail to Debbie (dlp@ast.cam.ac.uk).
 
You should generally book your \textbf{accommodation} yourself and claim this back on expenses. If you need any guidance while arranging travel, speak to Debbie who will be happy to advise.
 
\subsection{Observing Trips}
 
As part of your course, you may be required to visit overseas observatories to collect data and learn to use the facilities. For Home/EU students on STFC funding, STFC will generally pay for at least two nearby (La Palma) or one long-haul (Chile or Hawaii) observing trip.
 
If you are not a STFC student then funding for observing trips can be more problematic but not impossible. Some other sponsors can be persuaded to contribute but if there is no prospect of other sources of funding, and you have been admitted specifically for an observational project, the IoA will pay for two nearby (La Palma) or one long-haul (Chile or Hawaii) observing trips per student.
 
If your supervisor suggests you go observing, your first port of call, regardless of who is funding the trip, should be Debbie Peterson and/or Margaret Harding who will guide you through the practicalities.  Please note that students cannot go observing unaccompanied but are required to work with experienced (non-student) observers.
 
\subsection{Collaborative Visits}
 
Collaborative visits are less common than observing trips but occasionally, as part of your course, it may be desirable for you to work for a period of time at another institution. This might happen if your Supervisor is spending time working away from the Institute with a colleague at another institution or if another organisation provides specialised training in data handling or other techniques. For STFC funded students, collaborative visits are not paid for from conference funds but come from the pot of money which is allocated for `Fieldwork' (which also pays for observing trips). If your Supervisor suggests you make a collaborative visit please consult Debbie.
 
Non-STFC-funded students will need to ascertain whether their funding body is willing to cover the cost of collaborative visits to other institutions.
 

\paragraph{}
For Observing Trips and Collaborative Visits, you will be asked to complete a brief application form (available from Debbie) outlining purpose of visit and costs.


\subsection{Claiming Expenses}
 
To claim back your expenses, fill in an expense form available from the drawer opposite Debbie Peterson's office on the first floor of the Hoyle building (H12), attach {\bf all relevant receipts} and hand it to Debbie. You will generally receive the reimbursement within a couple of weeks.
 
If you do not have enough money to cover the expenses yourself before travelling you may apply for an advance. Use the appropriate form available in the same drawer, but make sure you apply in good time (\textit{i.e.} two weeks before you need it).
 
\subsection{Other Sources of Funding}
 
There are few supplementary sources for travel or maintenance support, but it is a good idea to join the Cambridge Philosophical Society (\textit{philosoc@hermes.cam.ac.uk}) which organises seminars and meetings and provides travel grants and translation grants. These grants are only available if you have been a member for more than 12 months\footnote{So the sooner you join the better!  E-mail them after you arrive and ask for an application form.}. The IoA does not provide funding for a continuation of your maintenance grant beyond the duration of your agreed period of studentship and there are very few other sources so this time-limit really should be your target. In certain circumstances the IoA may be able to help with maintenance costs for a few months \textbf{after} the submission of your thesis if there is a specific research project to which you will be contributing before moving on. Most Colleges provide some travel and/or book grants, as does the Institute of Physics if you're a member. Remember, it never hurts to ask!