%!TEX root = ./handbook.tex
\section{Potential Supervisors and their Research Interests}
\label{research}

Here is a list of the members of staff expected to be available as supervisors for students beginning PhDs in 2012, along with their office number and a brief summary of their research interests. Names in square brackets indicate people who are unlikely to take new students (about to take a sabbatical, retired, etc.) but may be available to talk to. Also included are the names of students currently working under each supervisor, along with a short description of their ongoing project. 

It is definitely recommended that you talk to the current students of a prospective supervisor before signing on, but do not read too much into the number of students any particular supervisor has! You can find the e-mail addresses of these people \fnurl{online}{http://www.ast.cam.ac.uk/people/} (unless their details are withheld) and on the IoA phone list (which you receive when you arrive here).

\subsection{Meet the Staff}

% ***************************************************AARSETH
\begin{person}{[Sverre Aarseth]}{H11}{./photos/aarseth}
    My main interests are: (i) direct N-body simulations of star clusters and (ii) development of related codes, including the use of GPUs for speeding up the calculation. It is now possible to study the evolution of a cluster containing 100,000 stars over 10 billion years. To be realistic, such simulations need to include an external tidal field, stellar evolution and primordial binaries. A particular project would be concerned with the study of black holes in globular clusters. This work requires the implementation of post-Newtonian terms which allow GR coalescence to be reached. The code can also be used to examine in detail fundamental dynamical processes as well as a rich variety of stellar evolution outcomes.
\end{person}

% ***************************************************BECKER
\begin{person}{George Becker}{K19}{./photos/becker.jpg}
    My research focuses on the high-redshift Universe, particularly the evolution of the intergalactic medium (IGM).  The IGM hosts the large majority of matter in the Universe.  High-resolution quasar spectra, together with numerical simulations, have demonstrated that the IGM is arranged in a ``cosmic web'' of filaments and voids, with galaxies forming in the densest regions.  I use quasar spectra to try and understand how the IGM evolves with time, and how galaxies and the IGM interact.  In particular, I study the processes by which hydrogen in the IGM is reionized by the first galaxies (we believe) and helium is reionized by quasars.  Recent projects have included charting the thermal history of the IGM, searching for the chemical signatures of the first galaxies, and measuring the total output of ionizing photons from all galaxies and quasars.
\end{person}

% ***************************************************BELOKUROV
\begin{person}{Vasily Belokorov}{H20}{./photos/belokurov.jpg}
    I am trying to figure out how matter, both visible and otherwise, is distributed in the Milky Way and around it. In galaxies, like our own, most of the matter seems to be dark (which makes it rather difficult to examine) and it appears to occupy huge cloud-like ``halos'' reaching out hundreds of kilo-parsecs. The halo is not completely dark -- there live quite a few pre-historic stars that fell into the Galaxy when it formed billions of years ago. These stars formed in smaller neighboring galaxies, at epochs equivalent to red-shifts of 6-10, and can tell us about the physical conditions in the Universe at that time. I study the properties of the Milky Way stellar halo in order to understand how the Galaxy formed and what role the ``dark matter'' played in the process. I also try to weigh other galaxies as well. For example, using the phenomenon called gravitational lensing, I measure the mass profiles of galaxy groups at high redshift. Galaxy groups are an important phase in structure formation that somehow avoided attention until recently. Overall, my research is a mixture of theory, observations and data mining. Speaking of data mining, I am also interested in machine learning techniques and their application in astronomy.

    \associated{Tom Collett}{Cosmology with strong Gravitational lenses.}
    \associated{Iulia Simion}{Building models of the inner Galaxy and study of the structures within, using optical and IR data from the SDSS, UKIDSS and VISTA  surveys.}
    \associated{Adam Bowden}{I work on constraining the potential of the Milky Way using tidal streams.}
    \associated{Simon Gibbons}{I work with Vasily and Wyn modelling streams, using them to see what we can learn about the Milky Way's potential.}
\end{person}

% ***************************************************CARSWELL
\begin{person}{[Bob Carswell]}{H16}{./photos/carswell.jpg}
    Observations of quasar spectra; physical conditions in quasars and active galaxies, proto-galaxies and the intergalactic medium.
\end{person}

% ***************************************************CHALLINOR
\begin{person}{Anthony Challinor}{K2}{./photos/challinor.jpg}
    I am interested in physical and observational cosmology and their role in constraining fundamental physics. Currently, my main research focus is on the theory, analysis and interpretation of the temperature anisotropies and polarization of the cosmic microwave background (CMB). I work on several projects with the Planck mission: searching for primordial gravitational waves via B-mode polarization; constraining cosmological models; weak gravitational lensing of the CMB; and constraining primordial non-Gaussianity and statistical anisotropy. I am also a member of the QUIJOTE CMB polarization experiment that will soon start surveying at radio frequencies to improve our knowledge of polarized Galactic foregrounds and measure CMB polarization.
\end{person}

% ***************************************************CLARKE
\begin{person}{Cathie Clarke}{H10}{./photos/clarke.jpg}
    I am involved in a range of projects related to star formation and circumstellar disc evolution mainly from a theoretical/computational perspective. Examples of recent studies include radiation hydrodynamical simulations of Xray photoevaporation of discs around young stars and simulations of stellar collisions within accreting proto-clusters. This year I have projects relating to both accretion discs and stellar dynamics. 

    \associated{Stacie Powell}{I am currently looking at the disk around the pre-main-sequence variable star FU Orionis, and analysing non-axisymmetric structure in the optical line profiles for comparison with the theoretical signature of a hot Jupiter.}
    \associated{Matt Young}{My research attempts to understand the development and outcome of gravitational instabilities in accretion discs, in particular under what conditions these instabilities can fragment into bound objects.  What this means is that I perform numerical simulations of proto-planetary discs and try and assess the likelihood that a runaway gravitational instability within the disc will form a planet.}
    \associated{Stefano Faccini}{In my research I am interested in protoplanetary discs, in particular I am now focusing on the dispersal of gaseous discs caused by the radiation of close-by hot stars.}
\end{person}

% ***************************************************EFSTATHIOU
\begin{person}{George Efstathiou}{K13}{./photos/efstathiou_new.jpg}
    Inflation and the early universe, cosmic microwave background radiation, scientific exploitation of the Planck Satellite.
\end{person}

% ***************************************************EVANS
\begin{person}{Wyn Evans}{H50}{./photos/evans.jpg}
    Galactic astronomy and dynamics; gravitational lensing; Solar System dynamics.

    \associated{Jonathan Diaz}{Evolution of dwarf galaxies, particularly the satellites of the Milky Way.}
\end{person}

% ***************************************************FABIAN
\begin{person}{Andy Fabian}{H23}{./photos/fabian.jpg}
    Clusters of galaxies; active galaxies; high energy astrophysics; X-ray astronomy.

    \associated{Erin Kara}{I study X-ray reverberation lags between the emission produced in the hot corona, and that which is reflected off of the accretion disc, in order to probe geometry and dynamics of the local environments of supermassive black holes.}
    \associated{Electra Panagoulia}{I am currently looking at a sample of (relatively) nearby X-ray galaxy clusters and studying their structural properties, in order to determine how cooling flows and heating work.}
    \associated{Michael Parker}{I use the NuSTAR X-ray telescope to probe the environment around active galactic nuclei and X-ray binaries.}
    \associated{Stephen Walker}{I study the X-ray emission from the outskirts of galaxy clusters to understand how clusters accrete matter as they continue to form.}
\end{person}

% ***************************************************GAIR
\begin{person}{Jonathan Gair}{H63}{./photos/gair.jpg}
    The existence of gravitational waves is an inevitable consequence of general relativity and was first predicted a century ago, but there have so far been no direct observations of gravitational waves by man-made detectors. This will change dramatically over the next decade. A network of ground-based gravitational wave detectors, including LIGO and Virgo, exists and the detectors are currently being upgraded to advanced configurations with sensitivity at which they are expected to make the first detections. There are plans for a low-frequency detector in space, LISA, that could fly in about a decade and will detect thousands of gravitational wave sources, beginning in earnest the era of gravitational wave astronomy. There are also ongoing efforts to detect gravitational waves with radio telescopes by accurate timing of networks of pulsars. My research is connected to source modelling and data analysis for all types of detectors and the scientific exploitation of the observations for astrophysics, cosmology and fundamental physics, including testing the theory of relativity. I also do some work in the mathematical theory of relativity and black holes.

    \associated{Steve Taylor}{I am investigating what gravitational-wave observations from merging neutron stars may tell us about the emitting source distribution, and the background cosmology. I am also interested in what a primordial gravitational-wave background may tell us about fundamental physics and early Universe conditions.}
    \associated{Rob Cole}{I study perturbed motion in extreme mass-ratio inspirals and how this relates to gravitational wave emission.}
    \associated{Stacey-Jo Parker}{I am currently looking at using Gravitational Wave observations to see what we can learn about the rate of Large Scale Structure formation in the Universe and consequently, the background Cosmology.} 
    \associated{Chris Moore}{I work with Dr Jonathan Gair on gravitational waves.}
\end{person}


% ***************************************************GILMORE
\begin{person}{Gerry Gilmore}{H47}{./photos/gilmore.jpg}
    GAIA, Gaia-ESO, galactic chemical evolution, near-field cosmology.
\end{person}

% ***************************************************GOUGH
\begin{person}{[Douglas Gough]}{H13}{./photos/gough.jpg}
    Astrophysical fluid dynamics; convection; stellar and solar pulsations.
\end{person}

% ***************************************************HAEHNELT
\begin{person}{Martin Haehnelt}{K27}{./photos/haehnelt.jpg}
    I am mainly interested in galaxy formation, the epoch of reionization and the large-scale distribution of baryons/neutrinos/dark matter.  Another interest is  the  formation and evolution of the central supermassive black holes in galaxies. Projects with me are generally theoretical in nature, but often involve the detailed modelling of observational data based  on numerical simulations. Joint supervision with one of the extra-galactic observers is also a possibility. You can find a brief overview of some of the projects I have recently worked on \fnurl{online.}{http://www.kicc.cam.ac.uk/presentations/Haehnelt_Kavliintro.pdf}

    \associated{Tiago Andre Costa}{I use large cosmological numerical simulations to investigate the growth of bright quasars at high redshift and to study the effects of their radiation on the surrounding environment.}
\end{person}

% ***************************************************HEWETT
\begin{person}{Paul Hewett}{H19}{./photos/hewett.jpg}
    Quasars and quasar populations; galaxy evolution; quasar absorbers; observational cosmology; gravitational lensing.

    Even by the standards of the IoA I have very broad, some would say eclectic(!), scientific interests, although the majority of my research has a link to the formation and evolution of galaxies and quasars/active-galactic-nuclei. Studies of the properties and evolution of object populations (which could be galaxies, gravitational lenses, high-redshift quasars, intervening absorber systems in quasars, signatures of quasar outflows,...) provide some of the most powerful constraints on theoretical models. Identifying such populations in large datasets/surveys in such a way that one really understands what one cannot, as well as what one can, detect forms the starting point for much of my research. Developing new statistical approaches to maximise the scientific output of surveys, particularly those involving optical/infrared spectra, is a particular current interest.
\end{person}

% ***************************************************HODGKIN
\begin{person}{Simon Hodgkin}{H39}{./photos/hodgkin.jpg}
    I study stars and stellar systems, particularly low mass and degenerate objects (white dwarfs and brown dwarfs). I'm particularly interested in trying to measure fundamental physical parameters (e.g. masses and radii) at young ages for very low mass stars, brown dwarfs and exoplanets. I use large format CCDs and infrared cameras to perform wide-angle, high-cadence, time-resolved photometry in open clusters (the Monitor project). I am also using large near-infrared surveys (with WFCAM and VISTA) to search for the coolest and faintest neighbours to the Sun.

    \associated{Gabor Kovacs}{I work in the ROPACS (ROcky Planets Around Cool Stars) project and the WFCAM transit survey.}
    \associated{Aimee Hall}{I'm looking for exoplanet transits from SuperWASP data that are hidden within the noise.}
\end{person}

% ***************************************************IRWIN
\begin{person}{Mike Irwin}{A1}{./photos/irwin.jpg}
    Survey astronomy: optical and near-infrared surveys, data analysis, near-field cosmology with Local Group galaxies.
\end{person}

% ***************************************************KENNICUTT
\begin{person}{Rob Kennicutt}{H49}{./photos/rkennicutt.jpg}
    Most of my research is focussed on observational extragalactic astronomy, in particular on studies of the structure, ISM, star formation, and evolution of galaxies.  Currently my activities are centred around a series of multi-wavelength Legacy projects on space and ground-based telescopes which I lead.  These include the Spitzer Infrared Nearby Galaxies Survey (SINGS), a multi-wavelength survey of 75 galaxies in the local supercluster, the Local Volume Legacy (LVL), a complete imaging survey with Spitzer, GALEX, and H-alpha of a sample in the local 11 Mpc volume, and the KINGFISH survey on the Herschel Space Observatory.  The common goals of these surveys are to carry out comprehensive surveys of the star formation, multi-phase ISM, and dust in nearby galaxies and to establish the fundamental properties and scaling laws that link star formation and the ISM.  I am also playing a leading role in a new integral-field spectroscopic survey of galaxies in the local Universe, the CALIFA survey, which will spectroscopically map 500 galaxies within z = 0.03.  These projects, which incorporate datasets spanning the entire electromagnetic spectrum, provide many opportunities for projects aimed at characterising star formation, chemical abundances, the ISM, and the evolution of nearby galaxies.
\end{person}

% ***************************************************LYNDEN-BELL
\begin{person}{[Donald Lynden-Bell]}{O13}{./photos/lyndenbell.jpg}
    My attitudes to Science are described in my article \fnurl{``Searching for Insight''}{http://dx.doi.org/10.1146/annurev-astro-081309-130859} on page 1 of 2010 Annual Reviews of Astronomy and Astrophysics.  My research has always been mathematical but led by the physics of some astronomical objects. I am mainly concerned with figuring out how real things work. e,g. What causes the large scale flows of galaxies in which all local galaxies move at some 600km/s relative to the local CMB.  I have had a lifelong interest in the origin of inertia and have worked within General Relativity to understand which relativistic models of the universe obey Mach's principle.  I believe in the power of mathematical analysis as an aid to understanding, and that computing the answers to physical problems gives relatively little insight on how the mechanism works.  My current interests include Relativistic Magnetohydrodynamic jets and the relativistic gravity of stress or pressure as well as the gravomagnetic forces due to matter currents. I also work on the optical design of wide field telescopes in collaboration with Dr Willstrop.
\end{person}

% ***************************************************MACKAY
\begin{person}{Craig Mackay}{O25}{./photos/mackay.jpg}
    The work of the Lucky Imaging group is to develop instruments that produce much higher resolution images of the sky than are possible with the Hubble Space Telescope.  These instruments use the latest electronic and software techniques to produce remarkable images.  A lot of our work can be found on the Lucky Imaging website (type ``lucky imaging'' into Google and press ``I'm feeling lucky''), including the highest resolution images ever taken anywhere on any telescope working in the visible or near infrared.  Students working in our group are always focused on an astronomical research programme which can be best carried out with lucky imaging techniques.  Students will get involved in different aspects of instrumental development.  This may involve hardware and/or software activities.  We normally mount an observing trip each year, most recently to Chile, Palomar (California) and La Palma (Canary Islands).  Our students are then closely involved in the design and construction of the instrument we take, work with the software we use to produce these remarkable images and then work on the astrophysics that those data address.

    \associated{Jonathan Crass}{I'm working on the AOLI project which combines the techniques of Lucky Imaging and Adaptive Optics to produce the highest resolution images every taken in astronomy at visible wavelengths.}
\end{person}

% ***************************************************MacTAVISH
\begin{person}{Carrie MacTavish}{K6}{./photos/mactavish.jpg}
    I am interested in all aspects of observational cosmology, from instrument design through to theory testing. I am currently involved in Spider, a balloon-borne polarimeter designed to target large scale CMB polarization. Spider is scheduled to launch from Australia in spring 2011. I am also involved in Planck, a satellite-based CMB telescope, which launched in spring 2009.
\end{person}

% ***************************************************Madhusudhan
\begin{person}{Nikku Madhusudhan}{H18}{./photos/madhusudhan.jpg}
    Observational surveys are revealing an extremely abundant and diverse population of exoplanetary systems, ranging from gas giants to earth-size planets in diverse environments. Recent advances in exoplanet observations and theory are leading to detailed characterization of exoplanetary atmospheres, interiors, and formation mechanisms. I am interested in all these aspects of exoplanetary characterization. One of my major areas of research is to characterize atmospheres of exoplanets using state-of-the-art observational facilities and detailed theoretical modeling and statistical retrieval techniques for interpreting exoplanetary spectra. Such efforts lead to constraints on the atmospheric chemical compositions, non-equilibrium processes, temperature inversions, atmospheric dynamics, and the presence of clouds/hazes, for the various classes of exoplanets, including gas giants, ice giants, and super-Earths. Secondly, I am also interested in understanding the interiors of Earth-size exoplanets and super-Earths, using detailed internal structure models of super-Earths and observations. Some new directions in this area include understanding new equations of state, mineralogy, and geophysical processes (e.g. plate tectonics, melt conditions, thermal evolution, etc.) for super-Earth interiors. A third major aspect of my research concerns using atmospheric compositions of exoplanets derived from exoplanetary spectra to place constraints on planet formation mechanisms and on chemical conditions in planet forming environments.
\end{person}

% ***************************************************McMAHON
\begin{person}{Richard McMahon}{K22}{./photos/mcmahon.jpg}
    My research is observational or experimental in nature in the broad area of galaxy formation and evolution at the highest redshifts and earliest times currently possible, especially in the Epoch of Reionization.  My main focus at the moment is on the discovery of new high redshift quasars powered by supermassive black holes, determination of their space densities and how this evolves with time, spectroscopic properties, host galaxies properties, star formation rates and how and when these high redshift quasars, their host galaxies and supermassive black holes form. These observational projects use a range of multi-wavelength observational techniques, primarily in the optical and near infra-red but also millimetre wavelengths.

    Potential projects that could start in 2011 will focus on the discovery of new quasars in the redshift range 5 to 7.5 using new data primarily from the recently started VISTA Hemisphere Survey and the Dark Energy Survey, a spectroscopic study of extremely red quasars, Herschel observations and deep Lyman-alpha studies of z=7 quasars.
\end{person}

% ***************************************************PARRY
\begin{person}{Ian Parry}{H57}{./photos/parry.jpg}
    Optical and IR instrumentation; redshift surveys; extra-solar planetary systems.

    \associated{Eleanor Bacchus}{I am working, as part of Project 1640, on detection and characterisation of exoplanets and brown dwarfs through direct imaging.}
\end{person}

% ***************************************************REES
\begin{person}{[Martin Rees]}{H3}{./photos/rees.jpg}
    Galaxy formation; high energy astrophysics; cosmology.
\end{person}

%****************************************************SIJACKI
\begin{person}{Deborah Sijacki}{K17}{./photos/sijacki.jpg}
    I am interested in the formation and evolution of cosmic structures from small mass galaxies at high redshifts to the most massive galaxy clusters of the present-day Universe. Structure formation is one of the most fascinating fields of astrophysics. Due to the non-linearity and large variety of physical phenomena occuring on a vast range of scales, it is very challenging to model theoretically, and the full complexity of these processes still needs to be unraveled. My primary research focus is on developing novel numerical models which can follow self-consistently the formation and growth of cosmic structures, including all of the three major constituents: dark energy, dark matter and baryons. In particular, I focus on hydrodynamical modelling of important astrophysical phenomena, such as active galactic nuclei, to understand how they influence the formation, growth and morphologies of the galaxies we observe today.

    \associated{Mike Curtis}{I study the accretion of matter onto AGN and the subsequent effects on host galaxies using cosmological hydrodynamic simulations.}
\end{person}


% ***************************************************TOUT
\begin{person}{Chris Tout}{H61}{./photos/tout.jpg}
    My research interests encompass every aspect of the theory of stellar evolution from the birth to death of stars, whether single, double or in clusters.  I construct detailed models with the Cambridge stellar evolution code, STARS as well as high speed empirical models for population synthesis and star cluster calculations.  Particular interests at present include the progenitors of all types of supernovae and gamma-ray bursts, stellar nucleosynthesis in stars and galactic chemical evolution, magnetic field evolution in white dwarfs and neutron stars, millisecond pulsars, evolution of orbital elements during mass transfer in binary systems and any observed stellar object that defies explanation or is of interest to a suitable student.

    \associated{Phil Hall}{Common-envelope evolution of binary star systems.}
    \associated{Sarah Smedley}{I study the formation of millisecond pulsars, testing the theory where the spins of normal pulsars are increased to millisecond timescales by accepting matter from a binary companion. Also, I am working on stellar dynamics with a view to exploring the effect runaway stars from clusters have on the IMF in the field.}
\end{person}

% ***************************************************TRENTI
\begin{person}{[Michele Trenti]}{K21}{./photos/mtrenti.jpg}
    My main research interest is the formation and evolution of galaxies at high-redshift, especially during the epoch of reionization at redshift z>6. I am a theorist with a growing interest in observations.  I lead the Brightest of Reionizing Galaxies (BoRG) survey, a large Hubble Space Telescope program aimed at searching rare galaxies at z$\sim$8. I am also actively working on dynamics of dense stellar systems, using direct N-body simulations and HST observations to identify signatures of intermediate mass black holes in globular clusters.
\end{person}

% ***************************************************VAN LEEUWEN
\begin{person}{Floor van Leeuwen}{O22}{./photos/vanleeuwen.jpg}
    My main interests are in the field of fundamental astronomy, obtaining reliable and accurate information that is used to help us better understand and constrain theories of star formation, stellar structure and stellar evolution. This involves large-scale determination of stellar parallaxes through satellite missions (Hipparcos, Gaia) to obtain luminosities; detailed studies of star clusters (both galactic and globular) to relate luminosities and colours to observational and theoretical isochrones; studies of double star systems to extract information on stellar masses and radii. Studies of local galactic dynamics are used to examine the dispersion of stellar populations. In a European collaboration as part of the GREAT project (led from Cambridge) I am involved in a a large-scale observational survey of star clusters to provide data that are complementary to the data we expect from Gaia, and that will help us to understand star clusters and their formation and evolution through studying the full 6D phase-space from observations, and comparing this with numerical simulations of these systems.

    My further interests concern dynamics of satellites, which for astrometric satellites such as Hipparcos and Gaia implies looking at very small effects from, for example, torques and thermal adjustments. In this context I am leading a development of a software package for the simulation of the Gaia satellite attitude.
\end{person}

% ***************************************************WALTON
\begin{person}{Nic Walton}{H37}{./photos/walton.jpg}
    Galactic structure and evolution, from large galactic surveys (in particular \fnurl{GAIA}{http://www.rssd.esa.int/gaia} and supported by ESO VST, VISTA). Large scale chemo-dynamical surveys of the Milky Way through the \fnurl{Gaia GREAT network}{http://www.great-esf.eu} to probe the chemical evolution of the Milky Way.  Core collapse supernovae and their use as probes of Dark Energy (this will be a focus of activity through the \fnurl{GREAT FP7 ITN}{http://www.great-itn.eu} - in collaboration with Microsoft and the World Wide Telescope Team). Emission line objects, e.g. Planetary Nebulae, as probes of galaxy chemical evolution - utilising the large \fnurl{IPHAS}{http://www.iphas.org} and VPHAS galactic plane survey data.

    Use of Virtual Observatory technologies applied to access of \fnurl{atomic and molecular data}{http://www.vamdc.eu}. Image analysis techniques and use breast cancer research, this work is in collaboration with the \fnurl{Dept of Oncology and Cambridge Research Institute}{http://www.pathgrid.org}.
\end{person}

% ***************************************************WYATT
\begin{person}{Mark Wyatt}{H38}{./photos/wyatt.jpg}
    My research covers topics related to the formation and evolution of planetary systems, with an equal mix of theory and observation, and application to both extrasolar systems (e.g., debris disks, extrasolar planets, proto-planetary disks) and to the Solar System (e.g., Kuiper belt, zodiacal cloud, irregular satellites). Theoretical studies mostly relate to planetary system dynamics, such as stability, planet-planetesimal interactions, planet formation, collisional evolution, dust physics, circumstellar disk formation. Observational studies mostly focus on detection of circumstellar dust, e.g., major projects I'm currently involved in include surveys for debris disks around the nearest 500 stars with Herschel and SCUBA2, with further observational projects using mid-IR, interferometry, coronagraphy and polarimetry.

    \associated{Alan Jackson}{I am interested in the formation and early evolution of planets, particularly giant impacts involving terrestrial planets and the evaporation of hot-Jupiters.}
    \associated{Tim Pearce}{I work on planetary dynamics in Mark Wyatt's group, and I'm currently looking at the interaction between a planet and a debris disk.}
\end{person}

\subsection{What Next?}

Following the identification of a primary supervisor, students will also be assigned a second supervisor who will normally be knowledgeable in the area of your research. Your second supervisor can assist with questions and sign forms when your primary supervisor is away, write you a reference when you come to applying for postdocs/jobs and generally act as another individual who is familiar with your work and able to help you towards your PhD.

As well as the academic staff don't forget that while they are not available as supervisors the many post-docs at the IoA and some of the emeritus staff are always around and generally happy to talk to you.  Indeed though they may not be your official supervisor you might end up working closely with some of them.  A list can be found \fnurl{on the IoA website.}{http://www.ast.cam.ac.uk/people/staff}.
