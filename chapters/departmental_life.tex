%!TEX root = ../handbook.tex
\section{Departmental Life}
\label{life}
 
One of the first things that you will discover about the Institute of Astronomy is that on the whole it is a very relaxed and friendly place. There are many opportunities to meet up with the other members of the department to discuss your work perhaps over a cup of tea. The department rightly emphasises the importance of discussion and the sharing of ideas. Indeed, this is always in your own interest as whatever problems you face in your academic work, it is most likely that there is someone here who is an expert in that particular field and might just be able to point you quickly in the right direction (see section 11.2 for a list of experts).
 
Don't feel that you are only allowed to talk to your supervisors about your work as you will soon discover that astronomers are a busy bunch who rarely seem to be able to stay in the same country for more than a few weeks at a time. If your supervisor is abroad and you have a particularly difficult problem with your work then it is always good to know to whom you can speak instead.
 
Academic work aside, there are also a number of occasions where you can get together with everyone from your fellow postgrads to the senior professors and just enjoy yourselves. In the following sections you can read about the most important events in your everyday life and how they are going to affect you.
 
\subsection{Geographical Issues}
 
The IoA is situated along Madingley Road, west of Cambridge city centre (see map). It takes about 10 minutes by bicycle or 25 minutes on foot to get to the Institute from the city centre. You can also take the Uni4 bus from Trumpington Street or Silver Street and alight (about 10 minutes later) on JJ Thomson Avenue.  For those with a university card, each ride on this bus between West Cambridge and the city centre currently costs 70p.  You can check the bus route (which does change at times) and schedule \fnurl{online.}{www.admin.cam.ac.uk/offices/em/travel/bus/uni4.shtml}.
 
The Institute comprises several buildings scattered throughout a wooded site (see the site map), making for a very pleasant working environment. The main entrance and reception are at the Hoyle Building. This was originally a long, single-storey building. In 1999, a modern lecture theatre -- the Raymond and Beverly Sackler lecture theatre -- was added. A second storey (the Corfield wing) and an extended entrance area were later established.
 
South of the Hoyle Building is the refurbished APM building\footnote{So-called because it used to house the Automated Plate Measuring facility, used to digitise and process photographic plates.} where the \fnurl{Cambridge Astronomical Survey Unit (CASU)}{http://casu.ast.cam.ac.uk} is based. The University Observatory, built in 1823, now houses the library and more offices. Room numbers of the form H26 are in the Hoyle building, whereas numbers like O26 refer to the Observatory.  To the East of the Hoyle building is the newer Kavli Institute for Cosmology, completed in 2009.  The Kavli comprises a programme of research projects involving the IoA, the Cavendish Astrophysics group and the Department of Applied Mathematics and Theoretical Physics (DAMTP).  The construction of the Kavli building formed the first phase of a longer term plan to relocate the entire Cavendish Astrophysics group to the IoA site. The next phase was the recently completed construction of the Battcock Centre, which now houses the Cavendish experimental astrophysicists, to the South of the Kavli building forming a rough court with the Kavli and Hoyle buildings.
 
All members of the IoA are issued with an external door key that accesses all the buildings and telescopes, which will be made available to you in your first week. This is subject to a \pounds 10 deposit. You may come and go as you please at all hours of the day and night (you are very unlikely to ever find the IoA deserted!). The University Security Patrol makes regular rounds of the site but it is very difficult for the Patrols to effectively police the site if they cannot identify the legitimate members of the IoA.  Please make sure you carry some form of identification (e.g. University card) at all times.
 
Although the University of Cambridge has no real centre and has buildings throughout much of central Cambridge, many of the science departments have, since the mid 70's, been gradually relocating to new sites in the west of Cambridge, conveniently close to the IoA. The Cavendish Laboratories and the computer laboratory, for instance, are situated just across Madingley Road from the IoA. Also nearby, on Wilberforce Road, is the Centre for Mathematical Sciences (CMS), where weekly cosmology lunches are held. DAMTP now forms part of the CMS.
 
\subsection{Postgraduate Lectures}
 
A key aspect of the IoA is the breadth of research undertaken and the familiarity of individuals with astrophysical topics that extend well beyond their own specific research specialities. Maintaining a broad overview of astrophysics as a whole has been key to the success of the IoA and, for PhD students and young postdoctoral researchers, resisting the all too common move to over-specialise has tangible
benefits for future careers.
 
The IoA, in colaboration with Cavendish Astrophysics, has a first-year programme of short, normally 8-lecture, courses designed to give IoA postgraduate students both a background knowledge of fundamental aspects of the subject and an introduction to a selection of highly topical research areas in astrophysics.  Attending lectures will also help you to make the most of the weekly seminars and colloquia at the Institute which you are expected to attend.  You also have the option of taking some of the Part III Mathematics courses provided in DAMTP, a number of which are given by IoA Staff.  These (typically) 24-lecture courses require a significant commitment in terms of both time and effort.  However, particularly for students embarking on certain theoretical PhD topics, attendance at the right course can provide an excellent grounding, of direct relevance to their research.

Attendance at the `computing' and `preprint presentation' courses is mandatory (approximately 15 hours) as all three will provide essential skills for your PhD studies and beyond. In addition, you will be expected to select courses totalling at least another 25 hours (a Part III course or two of the 8-lecture graduate courses for example). Your choice of courses can be discussed when you meet individually with Paul Hewett and Vasily Belokurov soon after you arrive.
 
The list of courses for 2013/2014 is:
\nopagebreak

\begin{tabular}{l}
Introduction to Computing, including Unix, Matlab, Python and IDL (Michaelmas Term)\\
Preprint Presentation Seminar Series (Michaelmas Term)\\
Cutting Edge Science with State-of-the-Art Techniques (Michaelmas Term)\\
Statistical Techniques and their Implementation in Python (Lent Term)\\
Introduction to IDL (Lent Term) \\
%Current Research Topics\\
%`Astro Nuts \& Bolts' (everything you need to know about how astronomy `works')\\
Astrophysical Techniques (Cavendish Astrophysics; Lent Term)\\
Interferometry (Cavendish Astrophysics; Lent Term)\\
\end{tabular}

The finalised and up-to-date schedule of lectures can be found on the \fnurl{IoA website.}{http://www.ast.cam.ac.uk/teaching/postgrad/current/lectures.php}


\subsection{Academic Events}
 
The IoA attracts many distinguished speakers from around the world to talk about their particular areas of expertise.  In addition there are a great many resident experts who are often all too happy to share their vast wealth of knowledge with the rest of us.
 
The main academic events in the week are the departmental colloquia and seminars. These are separate from the postgraduate lecture series, and are open to anyone who wants to listen.  It is strongly recommended that students attend (in fact, you'll be frowned upon if you don't).  One of the things of which you will constantly be reminded is that you should \textbf{not} just follow one narrow line of research, neglecting all other fields of study. This leads to a fairly na\"ive view of the subject as a whole, and will certainly put you in a very weak position when it comes to your viva exam and when applying for postdoctoral positions.
 
It is always worth checking what talks will be on that day by clicking the `talks' link on the IoA homepage.
 
%% It is possible to format this table more nicely in LaTeX, but then
%% latex2html has problems with it. For example, it does not understand
%% \tabularnewline. so one cannot use \raggedright for the last column.
\begin{table}[h!]
\begin{center}
  \begin{tabular}{|>{\raggedright}p{5.7cm}|l|c|>{\raggedright}p{6.2cm}|}\hline
 
    \textbf{Name} & \textbf{Day} & \textbf{Time} &
    \textbf{Location}\tabularnewline\hline\hline
 
    Institute of Astronomy Seminars &
    Wednesdays & 13:15 &
    Sackler Lecture Theatre, Hoyle Building\tabularnewline\hline
 
    Institute of Astronomy Astrophysics Colloquia &
    Thursdays & 16:00 &
    Sackler Lecture Theatre, Hoyle Building\tabularnewline\hline
 
    Cavendish Astrophysics Colloquia &
    Tuesdays & 16:30 &
    Ryle Seminar Room - Kavli Building\tabularnewline\hline
 
    Cavendish Astrophysics/ DAMTP/ IoA Joint Cosmology Lunches &
    Mondays & 13:00 &
    CMS Central Core, Rm.~5 (downstairs)\tabularnewline\hline
 
  \end{tabular}
\end{center}
\end{table}
 
\subsubsection{Wednesday Seminars}
 
These consist of a couple of short (half-an-hour maximum) talks given by resident and visiting astronomers on their current research. They are accompanied by a bread and cheese lunch at 12:30~pm, which is a veritable feast for a reasonable price (\pounds 3.00). Highly recommended. Though the official starting time is 12:30, most of the food will be gone by then, so it is advised to turn up 2 or 3 minutes early and get into a queue! The first year students used to have the pleasant responsibility of clearing up afterwards, but luckily (for you) this is now done by the department. Be warned that older students may feel slightly bitter about this fact!
 
During your time at the IoA, you will be required to give a Wednesday seminar about your work. It is expected that you will do this at some point during your second year, and again in your third year. You may also find it beneficial to present some results in your first year - this is good practice for conferences.
 
\begin{figure}[ht]
\centering
\includegraphics[width=0.99\columnwidth]{./photos/sackler.jpg}
\caption*{A talk in progress in the Sackler Lecture Theatre}
\end{figure}

\subsubsection{Thursday Colloquia}
On Thursday afternoons are the (more formal) astrophysics colloquia. The colloquium takes place in the Sackler lecture theatre at 4.00~pm. Afterwards there will be wine, port, fruit juice and a selection of nibbles (including cheese if you're lucky) - this is a good opportunity to informally quiz the speaker on their talk, or indeed anything else you can think of. This time, first year students are still responsible for washing up afterwards, on a rota system that they are responsible for organising. Often, the visiting speaker is taken out to dinner at one of Cambridge's restaurants in the evening, and you are encouraged to go along (and even bribed with a subsidy!). Thursday colloquia generally only run during Full Term (see section~\ref{reference}).
 
\subsubsection{Cavendish Colloquia}
The Cavendish Astrophysics group in the Cavendish Laboratories also run a series of colloquia. They are held at 4:30~pm every Tuesday afternoon during Full Term.  These are held in the Kavli Building. They are mostly advertised through the departmental mailing list, but it's worth keeping an eye on the display screen next to reception, which lists relevant talks throughout the university.
 
\subsubsection{Cosmology Lunches}
There is also a weekly cosmology lunch held down the road at the CMS (Centre for Mathematical Sciences) central core on Monday afternoons, which brings together cosmologists from the IoA, the Cavendish Laboratories, and the Department of Applied Maths and Theoretical Physics (DAMTP) for an informal social and scientific gathering and to chat about new developments in the field.  Students are welcome, and you can bring your own lunch or purchase it at the cafeteria. 
 
\subsubsection{Student Seminars}
In addition to the large departmental talks, there are lots of smaller, less formal discussion groups and talks which are held at varying times during the week. One lunchtime a fortnight (during Term) is usually set aside for student seminars, to which only the postgraduate students are invited. Each session a different person presents some of their own work in an extremely informal environment. Free cake and fruit juice are also provided. At the start of your time in Cambridge, one of the student seminars will consist of very brief presentations from the existing students to outline what they work on.

\begin{table}[th!]
\begin{center}
  \begin{tabular}{|>{\raggedright}p{4cm}|l|c|>{\raggedright}p{5cm}|>{\raggedright}p{3.2cm}|}\hline
 
    \textbf{Name} & \textbf{Day} & \textbf{Time} &
    \textbf{Location}& \textbf{Organiser}\tabularnewline\hline\hline

    Stars Group Meeting &
    Monday & 16:00 &
    Obs meeting room&
    Phil Hall\tabularnewline\hline
 
    Galaxies discussion group &
    Friday & 11.30 &
    Ryle meeting room&
    Martin Haehnelt\tabularnewline\hline

    Planets Journal Club &
    Thursday & 14:00 &
    Ryle meeting room&
    Clement Baruteau (c.baruteau@ damtp.cam.ac.uk)\tabularnewline\hline

    Student Seminars/ Journal Club &
    Time TBD &
    Obs Meeting Room/ Hoyle Committee Room&
    Matt Young\tabularnewline\hline

    X-ray Journal Club &
    Friday & 10:30 &
    Hoyle Commitee Room&
    Andy Fabian\tabularnewline\hline

    Mark Wyatt's Group Meeting &
    Tuesday & 14:00 &
    Obs meeting room&
    Mark Wyatt\tabularnewline\hline

    X-ray BunClub &
    Friday & 15:30 &
    Hoyle Commitee Room&
    Andy Fabian\tabularnewline\hline
 
  \end{tabular}
\end{center}
\caption{Journal clubs at the IoA}
\label{tab:jclubs}
\end{table}
 
\subsubsection{Journal clubs/Group Meetings}
Depending on what area you find yourself working in, there are also other journal clubs and meetings focused on one particular area of astrophysics. Most of these are relatively informal meetings, often involving food of some kind to stimulate the discussion process, of which the best-known example is probably the X-ray group BunClub.
 
New meetings and journal clubs are always springing up and in general people are very welcome to attend these, although asking the meeting chairman first is always best! A selection of these are listed in Table~\ref{tab:jclubs}.

\subsection{Outreach Activities}

The IoA has a diverse and ambitious public outreach programme, led by the outreach officer, Carolin Crawford (\textit{csc@ast.cam.ac.uk}). All graduate students are strongly encouraged to join in with some of the outreach activities, as they provide excellent practice and experience at communicating your subject -- which will serve you well when it comes to interviews, giving talks in your College, explaining what you study to your family, and of course, when you give professional seminars.  It's also fun, and can prove strongly motivational for your own research. 

\picright{5cm}{./photos/openday1.pdf}{
If you are interested in getting involved, there are plenty of opportunities.  Carolin is very happy to offer advice,  encouragement, images and general support for any talks that you give during your time here.  For more information on all activities, look at the \fnurl{Outreach page}{http://www.ast.cam.ac.uk/public/}.
}

\subsubsection{Public Open Evenings}

Open evenings are held every Wednesday night 7:00-9:00 pm from 2nd of October through to the end of March and are very popular with the local community (last year our average weekly attendance was around 210 people!). 

The evening begins with a 30-minute public talk, given by a post-doc or student from the IoA on any aspect of Astronomy at a very general level. This is followed by public observing or - in the more frequent case of cloudy weather - by tea.

\picright{5cm}{./photos/openday2.pdf}{
Graduate students are expected to help staff the evenings, and there is a modest remuneration for helping out as 'front of house' and with the observing. Open evening talks also provide a very friendly and supportive audience on which to practice your communication skills.  The sign-up rota is on the noticeboard between offices H16 and H17.  The IoA has a number of historical telescopes on site which are available for use, and you will receive instruction on their use as part of your first year training.
}

\subsubsection{Cambridge Science Festival/Open Day}

Every March we hold a large open afternoon and evening for the general public as part of the Cambridge Science Festival. Next year this will be on the afternoon of Saturday, the 22nd of March, so please keep that day free in your diary!

\picright{5cm}{./photos/openday3.pdf}{
The department is filled with displays and demonstrations, and everyone possible is expected to help out in some capacity. Over a thousand people attend from diverse backgrounds, and helping out on the day is always an interesting and entertaining experience. Afterwards there is pizza and beer for all volunteers.
}
 
\subsubsection{Press Releases}
When you do make that exciting newsworthy discovery, don't forget Carolin can also help organize press releases, including both the writing, and liaison with the very efficient University Press and Publications office. Even if you have a press release organized by another body (eg NASA, ESO), an accompanying CU press release is usually very successful in drawing local and national attention.
 
\subsubsection{Other Activities}
This is only the tip of the iceberg, there's much, much more going on.  For example why not,
 
\begin{itemize}
 
\item help host some of the many troops of brownies/cubs/scouts who visit us in the early evenings as part of their Astronomy badges? Again, modest remuneration is available for this activity
%\item join the Astropod crew in devising, performing, editing or producing the monthly IoA podcast? Talk to Susannah Alaghband-Zadeh for details. In the meantime check out \url{www.ast.cam.ac.uk/astropod/}
\item become one of the sages who answer some of the random questions that come in to the `Ask an Astronomer' website? Talk to Jonathan Crass.
 
\end{itemize}
 
Carolin is often asking for volunteers for various events - from visiting primary school careers fairs to hosting groups of sixth-formers who want to look round the department. And of course, if you have any great ideas for new posters, demonstrations, models, and outreach activities they're always welcome - just talk to Carolin.  She can (sometimes) be found in H22, and is always at the end of an email. 
 
\subsection{Not-so Academic Events}
 
The IoA has an arrangement with nearby Churchill College whereby any member of the IoA can use their tennis courts by simply going up to the lodge and asking for the key. Croquet and Volleyball Equipment are available for use in the IoA grounds.  In the summer there are occasional cut-throat  games of rounders, croquet, 5-a-side football and volleyball. Regardless of your research topic, you will always find a lot to occupy yourself with during the average week at the IoA. In fact, you will often find far \textbf{too much} to do, and it becomes difficult fitting in any work at all between coffee, lunch, lectures and other social gatherings.  Do make sure that you relax, and try to learn as much as you can about the wider world of Astronomy outside your chosen field of research. The IoA works very hard to build an active social atmosphere in the department, and you are strongly advised to make the best of it!
 
\subsubsection{Tea and Coffee}
A friendly, relaxed atmosphere is vital if you want to work efficiently, and it is under this dubious claim that we meet every (weekday!) morning at 11 o'clock for morning coffee\footnote{\ldots depending on your definition of coffee\ldots} and biscuits. This is normally held in the entrance to the Hoyle Building. It is well worth going to so that you can mix with your fellow students and get to know a bit about what they are doing, as well as steal some of their ideas. Most of the department will be there, so it is an ideal way to locate lost or well-concealed supervisors (and to show them that you've made it to work by 11 am). There is also an afternoon tea meeting at 3.30~pm, nearly identical to morning coffee.
 
The price for tea and coffee is \pounds 2 per month for students, to be paid to reception in instalments throughout the year (or in a lump sum if you prefer)\footnote{There is something of a vogue for seeing who can run up the largest unpaid tea bill. Be good and pay up.}. This is a much more civilised system than that run by some departments, where you have to pay for each cup! The cost covers not only the morning coffee and afternoon tea, but also the use of tea bags, sugar and milk in the kitchens. Coffee is apparently considered too dangerous to leave lying around, so you will have to bring your own if this is your thing. You can purchase coffee pods from the reception for the machine in the Hoyle kitchen at 32p per pod.
 
\subsubsection{Food}
There isn't a canteen at the IoA, but there are a number of other options around.  The William Gates Building (Computer Science) just across Madingley Road is very nearby and has a selection of sandwiches and other items.  At the far end of JJ Thomson avenue is West Cafe in the Hauser Forum building, a relatively new cafe that has a wider range of choice (including Costa Coffee), albeit (slightly!) further away.  There is also the Centre for Mathematical Sciences (CMS) on Wilberforce Road, quite handy if you are over there for a lecture or seminar already.  Should you so desire there is also a cafeteria in the Cavendish Laboratory, the fare is relatively standard for a big institutional canteen, not amazing but not going to give you food poisoning.  If you fancy a more substantial (i.e.\ liquid) lunch there are a couple of pubs on Huntingdon Road/Castle Street fairly close to the IoA, the Castle Inn being a particular department favourite, and of course the city centre is not that far away really.
 
There are also some lunch vans that stop at or near to the IoA.  Victoria's van arrives at the IoA every weekday, at variable times between 12.15 and 2.00~pm - you won't miss the horn!  It has a range of hot and cold sandwiches, salads, pots of pasta and often lasagna and baked potatoes; the van also serves fresh coffee.  Prices are usually between \pounds 2 and \pounds 3 for a sandwich or similar.  Every Tuesday and Thursday, a Nannamexico van comes to Clerk Maxwell Road (opposite the Obs building driveway) - they sell burritos, quesadillas, tacos and nachos (and it's all fresh, you watch them prepare it all); prices are around \pounds 5.  They have loyalty cards - with 10 stamps on your card, your next meal is free.  They are quite popular and do sell out of things so you are advised to get there before 12:30. Every Wednesday there is a steak van in the same spot on Clerk Maxwell Road, with a regular-sized meal costing around \pounds 5 again.
 
If you are in a nearby College you will be able to get lunch there if you want\footnote{Quality can be variable as can opinions of it!}. Churchill College kitchens fairly recently underwent a complete refurbishment and the dining hall welcomes visitors from nearby university departments. A hot meal costs in the region of \pounds 4 to \pounds 5.5, but salads and sandwiches are also available. They are open 12:30~--~13:30 and 18:15~--~19:00.
 
The IoA itself has vending machines that sell snacks and drinks. Dont forget Bread and Cheese Lunch every Wednesday during Term at 12.30 for just \pounds 3! There are also several small kitchens with a fridge and a microwave, so you can heat up your own food if you bring it. However, the kitchen near the Hoyle reception is only to be used by the catering staff for tea \&\ coffee, and should not be used by students, except for cleaning up after the Thursday colloquium.
 
\subsubsection{Holidays}
STFC states that ``research students may, with the prior agreement of their supervisors, take up to eight weeks holiday in each year (pro rata for parts of a year), inclusive of public holidays. Leave should not normally be taken during the academic term.'' However, this is quite a generous entitlement and students are asked to think carefully about how it would affect their progress before taking extended periods breaks. In any case, you should always check with your supervisor before making arrangements to be away from the Institute.
 
Do inform Bev at reception if you are away from the IoA for two days or less (this is mainly for fire safety etc). Make sure to keep Margaret Harding informed if you are planning a longer holiday.
