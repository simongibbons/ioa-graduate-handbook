IoA Graduate Handbook
=====================

The graduate handbook of the Institute of Astronomy, Cambridge University.

Getting the documents
---------------------

To get a full copy of the git repository so you can compile the handbook, run `git clone https://bitbucket.org/swt30/ioa-graduate-handbook.git` on your machine.

If you just want to grab a particular file, you will find a Source link just to the left if you are reading this on Bitbucket.

Compiling
---------

### Required packages ###

  All required packages should be available on the main IoA system, although possibly not on the X-ray computers.

### With latexmk ###

  If you have latexmk and the required packages installed, just type `make` on the command line from the root directory. 

  You can also use `make clean` to remove temporary files and `make preview` to run latexmk in continuous preview mode, where the files will be rebuilt every time you change them.

### Without latexmk ###

  Use `pdflatex handbook.tex` or your favourite flavour of LaTeX to compile.